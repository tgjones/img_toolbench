#include <algorithm>
#include <string>
#include <sstream>
#include <stdexcept>
#include <map>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <common/util.h>
#include <common/variant.h>
#include <common/algorithm.h>

tj::Algorithm::Algorithm()
{
    // Default flags and parameters
    setParameter("Quiet", true);
}

tj::Algorithm::Algorithm(Algorithm&& o)
    : _parameters(std::move(o._parameters))
{
}

tj::Algorithm& tj::Algorithm::operator = (tj::Algorithm&& o)
{
    _parameters = std::move(o._parameters);
    return *this;
}

void tj::Algorithm::setParameter(const std::string& name, const Variant& value)
{
    _parameters[name] = value;
}

const tj::Variant& tj::Algorithm::getParameter(const std::string& name) const
{
    if (_parameters.find(name) == _parameters.end())
    {
        std::stringstream ss;
        ss << "Parameter not found: " << name;
        throw std::out_of_range(ss.str());
    }
    return _parameters.at(name);
}

void tj::Pipeline::addAlgorithm(Algorithm& algorithm)
{
    _algorithms.push_back(&algorithm);
}

cv::Mat tj::Pipeline::operator()(const cv::Mat& I)
{
    // Recursively execute child algorithms
    cv::Mat result = I;
    for (std::list<Algorithm*>::iterator itr = _algorithms.begin();
        itr != _algorithms.end(); ++itr)
        result = std::move((*itr)->operator()(result));
    return result;
}

int tj::AlgorithmHarness::operator()(int argc, char** argv)
{
    if (_pipeline.empty())
        throw std::logic_error("Pipeline is empty");

    try
    {
        cv::Mat I;

        if (argc > 1)
        {
            I = cv::imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
            if (I.empty())
            {
                std::stringstream ss;
                ss << "Cannot read image `" << argv[1] << "'";
                throw std::runtime_error(ss.str().c_str());
            }
        }
        else
        {
            std::stringstream ss;
            ss << "No input image specified";
            throw std::invalid_argument(ss.str().c_str());
        }

        // Run pipeline
        cv::Mat results = _pipeline(I);

        // Save CSV
        std::stringstream csv;
        csv << argv[0] << ".csv";
        tj::csv(results, csv.str());

        // Save PNGestimateDetail
        std::stringstream png;
        png << argv[0] << ".png";
        cv::imwrite(png.str(), results);

        return 0;
    }
    catch (const std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return -1;
    }
}
