#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <common/util.h>

using namespace cv;

Mat tj::canny(const Mat& I, int lowThresh, int highThreshFactor, int blurKernelSize)
{
    CV_Assert(!I.empty());
    CV_Assert(I.channels() == 1);
    Mat edges(Size(I.rows, I.cols), CV_8U);
    blur(I, edges, Size(blurKernelSize,blurKernelSize));
    Canny(edges, edges, lowThresh, lowThresh * highThreshFactor, 3);
    return edges;
}

Mat tj::gray(const Mat& I, bool normalise)
{
    CV_Assert(!I.empty());
    Mat gray(Size(I.rows, I.cols), CV_8U);
    if (normalise)
        normalize(I, I, 0, 255, CV_MINMAX);
    I.convertTo(gray, CV_8U);
    return gray;
}

void tj::csv(const Mat& I, const std::string& fileName)
{
    CV_Assert(!I.empty());
    std::ofstream csv;
    csv.open(fileName.c_str());
    csv << cv::format(I, "csv");
    csv.close();
}

