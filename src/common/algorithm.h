#pragma once

#include <list>
#include <map>
#include <string>
#include <opencv2/core/core.hpp>

#include <common/variant.h>

namespace tj
{
    /**
     * Algorithm interface for building out other algorithms.
     *
     * This class is the fundamental building block within the project and use
     * of it and the special-case algorithm Pipeline, leads to a very flexible
     * system for quickly building or chaining larger algorithms consisting of
     * component parts.
     */
    class Algorithm
    {
    public:
        Algorithm();
        Algorithm(Algorithm&& o);

        Algorithm& operator = (Algorithm&& o);

        virtual ~Algorithm() {}

        virtual cv::Mat operator()(const cv::Mat& I) = 0;

        void           setParameter(const std::string& name, const Variant& param);
        const Variant& getParameter(const std::string& name) const;

    private:
        std::map<std::string, Variant> _parameters;
    };

    class Pipeline : public Algorithm
    {
    public:
        void   addAlgorithm(Algorithm& algorithm);

        void   clear() { _algorithms.clear();        }
        size_t size()  { return _algorithms.size();  }
        bool   empty() { return _algorithms.empty(); }

        cv::Mat operator()(const cv::Mat& I);

    private:
        std::list<Algorithm*> _algorithms;
    };

    class AlgorithmHarness
    {
    public:
        Pipeline& pipeline() { return _pipeline; }
        int operator()(int argc, char** argv);

    private:
        Pipeline _pipeline;
    };
}
