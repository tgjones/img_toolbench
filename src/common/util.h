#pragma once

#include <iostream>

#include <opencv2/core/core.hpp>

namespace tj
{
    cv::Mat canny(const cv::Mat& I, int lowThresh, int highThreshFactor = 3, int blurKernelSize = 3);
    cv::Mat gray(const cv::Mat& I, bool normalise = true);
    void csv(const cv::Mat& I, const std::string& fileName);
}
