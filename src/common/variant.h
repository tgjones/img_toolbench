#pragma once

namespace tj
{
    /**
     * Variant type wrapper.
     * As C++ is a strongly type language, this class uses as much magic as
     * is available to it to present a unified type wrapper.  Its primary goals
     * are ease of use and syntactic transparency, NOT efficiency.
     */
    class Variant
    {
    private:

        enum VariantType
        {
            VT_INTEGER,
            VT_SCALAR,
            VT_FLAG
        };

        union
        {
            int    _integer;
            double _scalar;
            bool   _flag;
        };

        VariantType _type;

    public:

        Variant() : _type(VT_INTEGER), _integer(0) {}

        // Implicit assignment operators (constructors not explicit)
        Variant(int integer)   : _type(VT_INTEGER), _integer(integer) {}
        Variant(double scalar) : _type(VT_SCALAR), _scalar(scalar) {}
        Variant(bool flag)     : _type(VT_FLAG), _flag(flag) {}

        // Magic casting operators for implicit conversion if no ambiguity
        operator int    () const { return this->asInteger(); }
        operator double () const { return this->asScalar(); }
        operator bool   () const { return this->asFlag(); }

        bool isInteger() const { return _type == VT_INTEGER; }
        bool isScalar() const  { return _type == VT_SCALAR; }
        bool isFlag() const    { return _type == VT_FLAG; }

        int    asInteger() const { assert(_type == VT_INTEGER); return _integer; }
        double asScalar() const  { assert(_type == VT_SCALAR); return _scalar; }
        bool   asFlag() const    { assert(_type == VT_FLAG); return _flag; }

        bool operator == (const Variant& other) const
        {
            if (_type == other._type)
            {
                switch (_type)
                {
                case VT_INTEGER: return _integer == other._integer;
                case VT_SCALAR:  return _scalar == other._scalar;
                case VT_FLAG:    return _flag == other._flag;
                }
                return true; // Never reached
            }
            else return false;
        }

        bool operator != (const Variant& other) const
        {
            if (_type == other._type)
            {
                switch (_type)
                {
                case VT_INTEGER: return _integer != other._integer;
                case VT_SCALAR:  return _scalar != other._scalar;
                case VT_FLAG:    return _flag != other._flag;
                }
                return false; // Never reached
            }
            else return true;
        }
    };
}
