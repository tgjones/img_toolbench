#pragma once

#include <string>
#include <sstream>
#include <memory>
#include <functional>
#include <unordered_map>

namespace tj
{
    template <typename I>
    class Factory
    {
    public:

        static Factory<I>& instance()
        {
            static Factory<I> factory;
            return factory;
        }

        typedef std::function<I*()> CreateFunc;

        void add(const std::string& name, CreateFunc func)
        {
            _types.emplace(name, func);
        }

        I* create(const std::string& name)
        {
            try
            {
                return _types.at(name)();
            }
            catch (std::out_of_range&)
            {
                std::stringstream ss;
                ss << "Factory cannot create `" << name << "'";
                throw std::runtime_error(ss.str().c_str());
            }
        }

    private:
        Factory() {}

        std::unordered_map<std::string, CreateFunc> _types;
    };

}
