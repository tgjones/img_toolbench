#pragma once

#include <string>
#include <lua.hpp>

namespace tj
{
    class LuaInterpreter
    {
    public:
        LuaInterpreter();
        ~LuaInterpreter();

        void runFile(const std::string& fileName);

    private:
        lua_State* _l;
    };
}
