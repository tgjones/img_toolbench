#include <lua.hpp>

#include <tools/script_host/lua_interpreter.h>

int main(int argc, char** argv)
{
    tj::LuaInterpreter().runFile(argv[1]);
}
