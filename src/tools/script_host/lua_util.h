#pragma once

#include <string>
#include <iostream>
#include <lua.hpp>

namespace tj
{
    inline void l_dumpStack (lua_State* l)
    {
        // Iterate down from top
        int top = lua_gettop(l);
        for (int i = top; i >= 1; i--)
        {
            int t = lua_type(l, i);
            std::cout << "(" << i << "," << i - top << ") " << lua_typename(l, t);
            switch (t)
            {
                case LUA_TSTRING:
                    std::cout << lua_tostring(l, i);
                    break;
                case LUA_TBOOLEAN:
                    std::cout << (lua_toboolean(l, i) != 0) ? "true" : "false";
                    break;
                case LUA_TNUMBER:
                    std::cout << lua_tonumber(l, i);
                    break;
                default:
                    break;
            }
            std::cout << std::endl;
        }
    }

    /**
     * Check userdata type and return pointer to it.
     */
    template <typename T>
    T* l_check_userdata(lua_State* l, int n, const std::string& id)
    {
        luaL_checktype(l, n, LUA_TUSERDATA);
        T* p = (T*)luaL_checkudata(l, n, id.c_str());
        assert(p != nullptr);
        return p;
    }

    /**
     * Create a new userdata instance and push it onth the stack.
     *
     * This function uses the placement new operator and lua_newuserdate to
     * construct a class instance and lets Lua manage its memory. To ensure the
     * destructor of the object is run prior to the garbage collector freeing
     * the memory this method's counterpart l_destruct_userdata should be called
     * from the userdata's '__gc' meta-method.
     */
    template <typename T, typename... Args>
    T* l_construct_userdata(lua_State* l, const std::string& id, Args... args)
    {
        void* udata = lua_newuserdata(l, sizeof(T));
        T* p = new(udata) T(args...);
        luaL_getmetatable(l, id.c_str());
        lua_setmetatable(l, -2);
        return p;
    }

    /**
     * Call destructor on userdata instance.
     *
     * This function explicitly calls the destructor on a Lua userdata instance
     * in order to prepare it for deallocation by the garbage collector. It
     * should be called from the '__gc' metamethod of a userdata, assuming the
     * counterpart function l_construct_userdata was used to initially allocate
     * the userdata instance (most likely in the Lua 'new' method).
     */
    template <typename T>
    void l_destruct_userdata(lua_State* l, const std::string& id)
    {
        // Call destructor but not delete, Lua manages actual memory
        T* p = l_check_userdata<T>(l, 1, id);
        p->~T();
    }
}
