#include <string>
#include <stdexcept>

#include <lua.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <common/algorithm.h>
#include <tools/script_host/api/lua_algorithm.h>
#include <tools/script_host/api/lua_pipeline.h>
#include <tools/script_host/lua_interpreter.h>

tj::LuaInterpreter::LuaInterpreter()
{
    _l = luaL_newstate();
    luaL_openlibs(_l);

    // Register API
    //tj::l_open_Algorithm(_l); // WORK IN PROGRESS
    tj::l_open_Pipeline(_l);
}

tj::LuaInterpreter::~LuaInterpreter()
{
    lua_close(_l);
}

void tj::LuaInterpreter::runFile(const std::string& fileName)
{
    int erred = luaL_dofile(_l, fileName.c_str());
    if (erred != 0)
    {
        std::stringstream ss;
        ss << "Lua error: " << luaL_checkstring(_l, -1);
        throw std::runtime_error(ss.str().c_str());
    }
}
