#pragma once

#include <lua.hpp>
#include <common/algorithm.h>

namespace tj
{
    /**
     * Initialises the Pipeline Lua API functions.
     */
    void l_open_Pipeline(lua_State* l);
}
