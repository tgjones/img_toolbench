// WORK IN PROGRESS

#include <lua.hpp>
#include <opencv2/core/core.hpp>

#include <common/algorithm.h>
#include <tools/script_host/lua_util.h>
#include <tools/script_host/api/lua_algorithm.h>

using namespace tj;

static const char* L_ALGORITHM_ID = "Algorithm";

cv::Mat LuaAlgorithm::operator () (const cv::Mat& I)
{
    if (hasLuaFunc())
    {
        lua_pushlightuserdata(_l, (void*)this);
        lua_gettable(_l, LUA_REGISTRYINDEX);
        // TODO: Convert I to Lua Image table
        lua_pcall (_l, 1, 1, 0);
        // TODO: Convert output to cv::Mat class
    }
    else throw std::runtime_error("No Lua function to run");
}

namespace api
{
    int l_Algorithm_new(lua_State* l)
    {
        l_construct_userdata<LuaAlgorithm, lua_State*>(l, L_ALGORITHM_ID, l);
        return 1;
    }

    int l_Algorithm_gc(lua_State* l)
    {
        l_destruct_userdata<LuaAlgorithm>(l, L_ALGORITHM_ID);
        return 0;
    }

    int l_Algorithm_call(lua_State* l)
    {
        LuaAlgorithm* algorithm =
            l_check_userdata<LuaAlgorithm>(l, 1, L_ALGORITHM_ID);
        if (!algorithm->hasLuaFunc())
            return luaL_argerror(l, -1, "no function to run");

        lua_rawgeti(l, LUA_REGISTRYINDEX, algorithm->getLuaFunc());

        // TODO: Get Lua Image image table from stack
        int err = lua_pcall(l, 1, 1, 0);
        if (err != LUA_OK)
            luaL_argerror(l, -1, "could not call algorithm func");
        // TODO: Push result Lua Image table onto stack
    }

    int l_Algorithm_setFunc(lua_State* l)
    {
        LuaAlgorithm* algorithm =
            l_check_userdata<LuaAlgorithm>(l, 1, L_ALGORITHM_ID);
        if (lua_isfunction(l, 2) != 0)
        {
            // Clear existing reference
            if (algorithm->hasLuaFunc())
                luaL_unref(l, LUA_REGISTRYINDEX, algorithm->getLuaFunc());

            // Save new function reference
            lua_pushvalue(l, 2);
            algorithm->setLuaFunc(luaL_ref(l, LUA_REGISTRYINDEX));
        }
        else return luaL_argerror(l, -1, "not a function");

        return 0;
    }

    int l_Algorithm_setParameter(lua_State* l)
    {
        return 0;
    }
}

void tj::l_open_Algorithm(lua_State* l)
{
    const luaL_Reg FUNCS[] = {
        { "new", api::l_Algorithm_new },
        { "__gc", api::l_Algorithm_gc },
        { "__call", api::l_Algorithm_call },
        { "setFunc", api::l_Algorithm_setFunc },
        { NULL, NULL} };

    // Register functions to meta-table
    luaL_getmetatable(l, L_ALGORITHM_ID);
    luaL_setfuncs(l, FUNCS, 0);

    // Meta-table is default index provider
    lua_pushvalue(l, -1);
    lua_setfield(l, -1, "__index");

    // Allow overrides in Lua
    lua_setglobal(l, L_ALGORITHM_ID);
}
