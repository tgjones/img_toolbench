// WORK IN PROGRESS

#pragma once

#include <lua.hpp>
#include <opencv2/core/core.hpp>

#include <common/algorithm.h>

namespace tj
{
    /**
     * Algorithm sub-class for the Lua world.
     *
     * This class provides a specialisation of the Algorithm interface for use
     * specifically within Lua.  It provides the the facility to create an
     * Algorithm userdata instance in Lua, and associate a Lua function with it.
     * The function is called when either the LuaAlgorithm's __call  meta-method
     * or the C++ call operator is executed, thus providing compatability with
     * the C++ recursive Pipeline design as well as the option for a Lua
     * specific use-case.
     */
    class LuaAlgorithm : public Algorithm
    {
    public:
        LuaAlgorithm(lua_State* l) : _l(l), _luaFunc(LUA_NOREF) {}
        cv::Mat operator () (const cv::Mat& I);

        bool hasLuaFunc() { return _luaFunc == LUA_NOREF; }
        void setLuaFunc(int luaFunc) { _luaFunc = luaFunc; }
        int getLuaFunc() { return _luaFunc; }

    private:
        lua_State* _l;
        int _luaFunc;
    };

    /**
     * Initialises the Algorithm Lua API functions.
     */
    void l_open_Algorithm(lua_State* l);
}
