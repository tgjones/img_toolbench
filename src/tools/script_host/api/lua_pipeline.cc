#include <cmath>
#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <typeinfo>
#include <limits>

#include <lua.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <common/factory.h>
#include <common/algorithm.h>
#include <algorithms/apply_radial_weight.h>
#include <algorithms/compute_psd.h>
#include <algorithms/detect_edges.h>
#include <algorithms/estimate_detail.h>
#include <tools/script_host/lua_util.h>
#include <tools/script_host/api/lua_algorithm.h>
#include <tools/script_host/api/lua_pipeline.h>

using namespace tj;

static const char* L_PIPELINE_ID = "Pipeline";

namespace api
{
    /**
     * API Pipeline constructor.
     */
    int l_Pipeline_new(lua_State* l)
    {
        l_construct_userdata<Pipeline>(l, L_PIPELINE_ID);
        return 1;
    }

    int l_Pipeline_gc(lua_State* l)
    {
        l_destruct_userdata<Pipeline>(l, L_PIPELINE_ID);
        return 0;
    }

    /**
     * API meta-method for calling a Pipeline like a function.
     *
     * The call meta-method allows the Pipeline instance to be 'called' directly
     * from inside Lua using normal function syntax, and is really a simple
     * wraps the C++ call operator which recursively executes the pipeline
     * algorithms.
     */
    int l_Pipeline_call(lua_State* l)
    {
        Pipeline& pipeline = *l_check_userdata<Pipeline>(l, 1, L_PIPELINE_ID);
        std::string inputFileName = luaL_checkstring(l, 2);
        cv::Mat I = cv::imread(inputFileName, CV_LOAD_IMAGE_GRAYSCALE);
        if (I.empty())
        {
            std::stringstream ss;
            ss << "cannot read image `" << inputFileName << "'";
            throw std::runtime_error(ss.str().c_str());
        }

        // Run pipeline
        cv::Mat results = pipeline(I);

        return 0;
    }

    /**
     * API method for appending an algorithm to a Pipeline.
     */
    // TODO: Accept LuaAlgorithm or just a raw function.
    int l_Pipeline_append(lua_State* l)
    {
        Pipeline& pipeline = *l_check_userdata<Pipeline>(l, 1, L_PIPELINE_ID);
        std::string algorithmName = luaL_checkstring(l, 2);

        Algorithm* algorithm =
            Factory<Algorithm>::instance().create(algorithmName);

        // Parse paramters
        lua_pushnil(l);
        while (lua_next(l, 3))
        {
            std::string parameterName = luaL_checkstring(l, -2);
            if (lua_isboolean(l, -1))
            {
                // Flag
                algorithm->setParameter(parameterName, lua_toboolean(l, -1) != 0);
            }
            else if (lua_isnumber(l, -1))
            {
                double val = lua_tonumber(l, -1);
                if (std::fmod(val, 1.0f) > std::numeric_limits<double>::epsilon())
                {
                    // Scalar
                    algorithm->setParameter(parameterName, val);
                }
                else
                {
                    // Integer
                    algorithm->setParameter(parameterName, static_cast<int>(val));
                }
            }
            else
            {
                std::stringstream ss;
                ss << "bad value for `" << parameterName << "'";
                return luaL_argerror(l, -1, ss.str().c_str());
            }
            lua_pop(l, 1);
        }

        // Save for auto destruction
        static std::vector<std::unique_ptr<Algorithm>> algorithms;
        algorithms.push_back(std::unique_ptr<Algorithm>(algorithm));

        pipeline.addAlgorithm(*algorithm);

        return 0;
    }
}

void tj::l_open_Pipeline(lua_State* l)
{
    const luaL_Reg FUNCS[] = {
        { "new", api::l_Pipeline_new },
        { "__gc", api::l_Pipeline_gc },
        { "__call", api::l_Pipeline_call },
        { "process", api::l_Pipeline_call },
        { "append", api::l_Pipeline_append },
        { NULL, NULL } };

    // Register functions to meta-table
    luaL_newmetatable(l, L_PIPELINE_ID);
    luaL_setfuncs(l, FUNCS, 0);

    // Meta-table is default index provider
    lua_pushvalue(l, -1);
    lua_setfield(l, -1, "__index");

    // Allow overrides in Lua
    lua_setglobal(l, L_PIPELINE_ID);

    // Setup Algorithm factory string associations
    Factory<Algorithm>::instance().add("ApplyRadialWeight",
        []() { return new ApplyRadialWeight(); });
    Factory<Algorithm>::instance().add("ComputePsd",
        []() { return new ComputePsd(); });
    Factory<Algorithm>::instance().add("DetectEdges",
        []() { return new DetectEdges(); });
    Factory<Algorithm>::instance().add("EstimateDetail",
        []() { return new EstimateDetail(); });
}
