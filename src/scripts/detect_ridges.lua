computePsdParams = {
    ["Quiet"] = false
}

detectEdgesParams = {
    ["Quiet"] = false,
    ["LowThreshTrackbarMax"] = 40,
    ["LowThresh"] = 27,
    ["HighThreshFactor"] = 2,
    ["BlurKernelSize"] = 5
}

local detectRidges = Pipeline.new("DetectRidges")
detectRidges:append("ComputePsd", computePsdParams);
detectRidges:append("DetectEdges", detectEdgesParams);
detectRidges("../img/lena_grey.jpg");
