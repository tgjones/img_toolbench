#include <cmath>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithms/apply_radial_weight.h>

cv::Mat tj::ApplyRadialWeight::operator()(const cv::Mat& I)
{
    cv::Mat weightMap = cv::Mat::zeros(I.rows, I.cols, CV_32F);

    // Note: in even dimensions the centre is off by 1
    cv::Point centre = cv::Point(I.cols / 2, I.rows / 2);

    for (int row = 0; row < I.rows; row++)
       for (int col = 0; col < I.cols; col++)
            weightMap.at<double>(row, col) = 
                std::sqrt(std::pow(static_cast<double>(row) - static_cast<double>(centre.y), 2) 
                    + std::pow(static_cast<double>(col) - static_cast<double>(centre.x), 2));

    cv::normalize(weightMap, weightMap, 0.0f, 1.0f, CV_MINMAX);

    cv::Mat weighted;
    cv::multiply(I, weightMap, weighted);
    return weighted;
}
