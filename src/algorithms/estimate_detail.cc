#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <common/util.h>
#include <algorithms/compute_psd.h>
#include <algorithms/detect_edges.h>
#include <algorithms/estimate_detail.h>

using namespace cv;

static const std::string CANNY_WINDOW_NAME   = "Canny Edges";
static const std::string CANNY_TRACKBAR_NAME = "Low Threshold";

Mat tj::EstimateDetail::operator()(const Mat& I)
{
    int blockRows = I.rows / getParameter("BlockSize").asInteger() + 1;
    int blockCols = I.cols / getParameter("BlockSize").asInteger() + 1;

    // Pad image accomodate whole number of blocks
    Mat padded;
    int extra_cols = getParameter("BlockSize").asInteger() - (I.cols % getParameter("BlockSize").asInteger());
    int extra_rows = getParameter("BlockSize").asInteger() - (I.rows % getParameter("BlockSize").asInteger());
    copyMakeBorder(I, padded, 0, extra_rows, 0, extra_cols, BORDER_CONSTANT, Scalar::all(0));

    if (!getParameter("Quiet"))
    {
        imshow("Padded Image", padded);
        waitKey();
    }

    tj::ComputePsd computePsd;
    tj::DetectEdges detectEdges;

    detectEdges.setParameter("LowThreshTrackbarMax", 40.0f);
    detectEdges.setParameter("LowThresh",            27.0f);
    detectEdges.setParameter("HighThreshFactor",     2.8f);
    detectEdges.setParameter("BlurKernelSize",       5.0f);   

    cv::Mat psds = Mat::zeros(padded.rows, padded.cols, CV_32F);
    cv::Mat ridges = Mat::zeros(padded.rows, padded.cols, CV_8U);

    for (int row = 0; row < blockRows; row++)
    for (int col = 0; col < blockCols; col++)
    {
        // Get block
        Rect block;
        block.x = col * getParameter("BlockSize").asInteger();
        block.y = row * getParameter("BlockSize").asInteger();
        block.width  = getParameter("BlockSize").asInteger();
        block.height = getParameter("BlockSize").asInteger();

        // Compute PSD and ridges
        computePsd(padded(block)).copyTo(psds(block));
        detectEdges(psds(block)).copyTo(ridges(block));
    }

    if (!getParameter("Quiet"))
    {
        cv::imshow("PSD Blocks", tj::gray(psds));
        cv::imshow("PSD Ridges", ridges);
        cv::waitKey();
    }

    return ridges; // TODO: Should be returning wear!
}
