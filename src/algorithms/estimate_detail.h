#pragma once

#include <opencv2/core/core.hpp>

#include <common/algorithm.h>

namespace tj
{
    class EstimateDetail : public Algorithm
    {
    public:
        cv::Mat operator ()(const cv::Mat& I);
    };
}
