#pragma once

#include <common/algorithm.h>

namespace tj
{
    class ApplyRadialWeight : public Algorithm
    {
    public:
        cv::Mat operator()(const cv::Mat& I);
    };
}
