#include <vector>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <common/util.h>
#include <algorithms/compute_psd.h>
#include <algorithms/detect_edges.h>

static const std::string CANNY_WINDOW_NAME   = "Canny Edges";
static const std::string CANNY_TRACKBAR_NAME = "Low Threshold";

void tj::DetectEdges::cannyTrackbarCB(int pos, void* data)
{
    DetectEdges* de = static_cast<DetectEdges*>(data);
    de->_cannyImg = tj::canny( de->_grayImg, pos, 
        de->getParameter("HighThreshFactor"), de->getParameter("BlurKernelSize"));
    cv::imshow(CANNY_WINDOW_NAME, de->_cannyImg);
}

cv::Mat tj::DetectEdges::operator()(const cv::Mat& I)
{
    _grayImg = tj::gray(I);

    _cannyImg = tj::canny(_grayImg, getParameter("LowThresh"), getParameter("HighThreshFactor"), 
        getParameter("BlurKernelSize"));

    if (!getParameter("Quiet"))
    {
        cv::namedWindow(CANNY_WINDOW_NAME, CV_WINDOW_AUTOSIZE);
        cv::imshow(CANNY_WINDOW_NAME, _cannyImg);
        cv::createTrackbar(CANNY_TRACKBAR_NAME, CANNY_WINDOW_NAME, NULL, getParameter("LowThreshTrackbarMax"), 
            cannyTrackbarCB, static_cast<void*>(this));
        cv::setTrackbarPos(CANNY_TRACKBAR_NAME, CANNY_WINDOW_NAME, getParameter("LowThresh"));        
    }

    if (!getParameter("Quiet"))
        cv::waitKey();

    return _cannyImg;      
}
