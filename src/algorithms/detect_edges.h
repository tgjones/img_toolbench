#pragma once

#include <string>
#include <opencv2/core/core.hpp>

#include <common/algorithm.h>

namespace tj
{
    class DetectEdges : public Algorithm
    {
    public:
        cv::Mat operator()(const cv::Mat& I);

    private:
        static void cannyTrackbarCB(int pos, void* data);

        cv::Mat _grayImg;
        cv::Mat _cannyImg;
    };
}
