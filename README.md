Image Processing Toolbench
==========================

The purpose of this project is to provide a flexible, and fast, toolbench for 
image processing prototyping and research.  In particular the toolbench is 
intended to offer an alternative to a higher level prototyping environment (such 
as MATLAB) for people more comfortable with traditional coding practices.

Components
----------

### Libraries
The following components compile as static libraries:

 - _Common_  
   This library contains utility classes and functions used by other parts of 
   the framework.

 - _Algorithms_  
   This library contains all the image processing algorithms built into the 
   framework, these can be easily extended or chained togother in a pipeline to
   achive your prototyping goal. See the [Algorithms](#Algorithms) section below 
   for an explanation of the frameowrk design and instructions on how to build 
   your own algorithms.

### Tools
The following components compile as binary executables:

 - _Script Host_  
   This tool integrates the [Lua](http://www.lua.org) syntax interpreter, which 
   provides access to all the Lua standard libraries as well as a custom API for 
   using the Image Processing Toolbench.  For pure prototyping, using the Lua 
   interface is preferable to using the C++ API due to the rapid productivity 
   cycle afforded by using a high level scripting language.  See the 
   [Lua Prototyping](#Writing Lua Scripts) section below for an explanation of 
   how the Lua API maps to the C++ API, its benefits and drawbacks and 
   instructions on how to script your own prototypes.

Algorithms
----------

Dependencies
------------

 - OpenCV 2.8.4
 - Lua 5.2.1

Buillding
---------
