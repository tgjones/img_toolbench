#!/bin/bash
if [[ $OSTYPE == cygwin* ]]; then
    ./build.cmd
else
    mkdir -p build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=DEBUG
    make
    status=$?
fi
cd ..
exit $status
