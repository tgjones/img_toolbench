@echo off
:: Depends on VS2012 Update 3 with Nov 2012 CTP and CMake 2.8
call "%VS110COMNTOOLS%\vsvars32.bat"
call "%VCINSTALLDIR%\vcvarsall.bat" x86_amd64
if not exist build mkdir build
cd build
"%ProgramFiles(x86)%\CMake 2.8\bin\cmake" -G "Visual Studio 11 Win64" -T "v120_CTP_Nov2012" ..
msbuild /nologo /clp:NoSummary ALL_BUILD.vcxproj
cd ..
